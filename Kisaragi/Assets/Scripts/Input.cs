﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Input : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerUpHandler {

    public int y;
    public int x;
	public Cell cell;

	// Use this for initialization
	void Start () {
		cell = GetComponent<Cell> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("ぽいんたーだうん");

		BoardManager.Instance.TapHighlight(y, x, cell);
    }

    public void OnPointerUp(PointerEventData eventData)
    {

    }

    public void OnPointerEnter(PointerEventData eventData)
    {
    }

    public void SetCordinate (int y, int x)
    {
        this.y = y;
        this.x = x;
    }

}
