﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cell : MonoBehaviour {

	public enum Koma
    {
        Gyoku,
        Kin,
        Gin,
        Kei,
        Kyou,
        Hisha,
        Kaku,
        Hu,
        None
    };

    public enum KomaState
    {
        Black,      // 先手
        White,      // 後手
        None        // 何も置かれていない
    };

	public enum KomaPlace
	{
		Board,		// 盤上
		Table		// 持ち駒台上
	}

    public Koma koma;
    public KomaState komaState;
	public KomaPlace komaPlace;
    public bool isPromoted;
    public int yIndex;
    public int xIndex;

    private Cell cell;

    #region Start
    private void Start()
    {
        //cell = this.gameObject.GetComponent<Cell>();
        
        //isPromoted = false;
        //komaState = KomaState.None;

        //string tag;
        //tag = this.gameObject.tag;
        //switch (tag)
        //{
        //    case "Hu":
        //        cell.komaState = KomaState.Hu;
        //        break;
        //    case "Hisha":
        //        cell.komaState = KomaState.Hisha;
        //        break;
        //    case "Kaku":
        //        cell.komaState = KomaState.Kaku;
        //        break;
        //    case "Kyou":
        //        cell.komaState = KomaState.Kyou;
        //        break;
        //    case "Kei":
        //        cell.komaState = KomaState.Kei;
        //        break;
        //    case "Gin":
        //        cell.komaState = KomaState.Gin;
        //        break;
        //    case "Kin":
        //        cell.komaState = KomaState.Kin;
        //        break;
        //    case "Gyoku":
        //        cell.komaState = KomaState.Gyoku;
        //        break;
        //    default:
        //        cell.komaState = KomaState.None;
        //        break;
        //}
    }
    #endregion

    public void Init()
    {
        cell = this.gameObject.GetComponent<Cell>();
        cell.koma = Koma.None;
        cell.komaState = KomaState.None;
		cell.komaPlace = KomaPlace.Board;
        cell.isPromoted = false;
        yIndex = 0;
    } 

	public Cell SetCellInitialize (Koma koma, KomaState komaState, bool isPromoted, int yIndex, int xIndex, KomaPlace komaPlace = KomaPlace.Board)
    {
        cell = this.gameObject.GetComponent<Cell>();

        cell.koma = koma;
        cell.komaState = komaState;
		cell.komaPlace = komaPlace;
        cell.isPromoted = isPromoted;
        cell.yIndex = yIndex;
        cell.xIndex = xIndex;

        return cell;
    }

	public Cell SetCell(Koma koma, KomaState komaState, bool isPromoted, KomaPlace komaPlace = KomaPlace.Board)
    {
        cell = this.gameObject.GetComponent<Cell>();

        cell.koma = koma;
        cell.komaState = komaState;
		cell.komaPlace = komaPlace;
        cell.isPromoted = isPromoted;

        return cell;
    }

	public void Promote(bool isPromoted) {
		cell = this.gameObject.GetComponent<Cell>();
		cell.isPromoted = isPromoted;
	}

	// 指した後、成れる場所かどうか判定
	#region CheckPromoteZone
	public bool CheckPromoteZone () {
		switch (GameManager.Instance.turn) {
		case GameManager.Turn.Black:
			if (cell.yIndex <= 2 && cell.yIndex > 0)
				return true;
			else
				return false;
		case GameManager.Turn.White:
			if (cell.yIndex < 8 && cell.yIndex >= 6)
				return true;
			else 
				return false;
		default:
			return false;
		}
	}
	#endregion

    #region MovablePoints
	public List<Cell> MovablePoints (int y, int x, Cell cell)
    {
        Cell[,] cells = BoardManager.Instance.cells.cells;

        List<Cell> movablePoints = new List<Cell>();
        Koma koma = cell.koma;
        KomaState komaState = cell.komaState;
        bool isPromoted = cell.isPromoted;
        int direction = 0;
		int frontLine = 0;
        
		if (komaState == KomaState.Black) {
			direction = -1;
			frontLine = 8;
		} else if (komaState == KomaState.White) {
			direction = 1;
			frontLine = 0;
		}

		if (cell.komaPlace == KomaPlace.Board) {
			switch (koma) {
				#region Hu
			case Koma.Hu:
				if (!isPromoted) {
					if (cells [y + direction, x].komaState != komaState) {
						movablePoints.Add (cells [y + direction, x]);
					}
				} else {
					if ((y + direction) >= 0 && (y + direction) <= 8) {
						if (cells [y + direction, x].komaState != komaState) {
							movablePoints.Add (cells [y + direction, x]);
						}
						if (x < 8) {
							if (cells [y + direction, x + 1].komaState != komaState) {
								movablePoints.Add (cells [y + direction, x + 1]);
							}
						}
						if (x > 0) {
							if (cells [y + direction, x - 1].komaState != komaState) {
								movablePoints.Add (cells [y + direction, x - 1]);
							}
						}
					}
					if ((y - direction) >= 0 && (y - direction) <= 8) {
						if (cells [y - direction, x].komaState != komaState) {
							movablePoints.Add (cells [y - direction, x]);
						}
					}
					if (x + 1 <= 8) {
						if (cells [y, x + 1].komaState != komaState) {
							movablePoints.Add (cells [y, x + 1]);
						}
					}
					if (x - 1 >= 0) {
						if (cells [y, x - 1].komaState != komaState) {
							movablePoints.Add (cells [y, x - 1]);
						}
					}
				}
				break;
				#endregion
				#region Kyou
			case Koma.Kyou:
				if (!isPromoted) {
					for (int i = 0; (y + i <= 8) && (y + i >= 0); i += direction) {
						if (i == 0) {
							continue;
						}

						if (cells [y + i, x].komaState != komaState) {
							movablePoints.Add (cells [y + i, x]);
							break;
						} else {
							break;
						}
					}
				} else {
					if ((y + direction) >= 0 && (y + direction) <= 8) {
						if (cells [y + direction, x].komaState != komaState) {
							movablePoints.Add (cells [y + direction, x]);
						}
						if (x < 8) {
							if (cells [y + direction, x + 1].komaState != komaState) {
								movablePoints.Add (cells [y + direction, x + 1]);
							}
						}
						if (x > 0) {
							if (cells [y + direction, x - 1].komaState != komaState) {
								movablePoints.Add (cells [y + direction, x - 1]);
							}
						}
					}
					if ((y - direction) >= 0 && (y - direction) <= 8) {
						if (cells [y - direction, x].komaState != komaState) {
							movablePoints.Add (cells [y - direction, x]);
						}
					}
					if (x + 1 <= 8) {
						if (cells [y, x + 1].komaState != komaState) {
							movablePoints.Add (cells [y, x + 1]);
						}
					}
					if (x - 1 >= 0) {
						if (cells [y, x - 1].komaState != komaState) {
							movablePoints.Add (cells [y, x - 1]);
						}
					}
				}
                
				break;
				#endregion
				#region Kei
			case Koma.Kei:
				if (!isPromoted) {
					if (x < 8) {
						if (cells [y + (2 * direction), x + 1].komaState != komaState) {
							movablePoints.Add (cells [y + (2 * direction), x + 1]);
						}
					}
					if (x > 0) {
						if (cells [y + (2 * direction), x - 1].komaState != komaState) {
							movablePoints.Add (cells [y + (2 * direction), x - 1]);
						}
					}
				} else {
					if ((y + direction) >= 0 && (y + direction) <= 8) {
						if (cells [y + direction, x].komaState != komaState) {
							movablePoints.Add (cells [y + direction, x]);
						}
						if (x < 8) {
							if (cells [y + direction, x + 1].komaState != komaState) {
								movablePoints.Add (cells [y + direction, x + 1]);
							}
						}
						if (x > 0) {
							if (cells [y + direction, x - 1].komaState != komaState) {
								movablePoints.Add (cells [y + direction, x - 1]);
							}
						}
					}
					if ((y - direction) >= 0 && (y - direction) <= 8) {
						if (cells [y - direction, x].komaState != komaState) {
							movablePoints.Add (cells [y - direction, x]);
						}
					}
					if (x + 1 <= 8) {
						if (cells [y, x + 1].komaState != komaState) {
							movablePoints.Add (cells [y, x + 1]);
						}
					}
					if (x - 1 >= 0) {
						if (cells [y, x - 1].komaState != komaState) {
							movablePoints.Add (cells [y, x - 1]);
						}
					}
				}
				break;
				#endregion
				#region Gin
			case Koma.Gin:
				if (!isPromoted) {
					if ((y + direction) >= 0 && (y + direction) <= 8) {
						if (cells [y + direction, x].komaState != komaState) {
							movablePoints.Add (cells [y + direction, x]);
						}
						if (x < 8) {
							if (cells [y + direction, x + 1].komaState != komaState) {
								movablePoints.Add (cells [y + direction, x + 1]);
							}
						}
						if (x > 0) {
							if (cells [y + direction, x - 1].komaState != komaState) {
								movablePoints.Add (cells [y + direction, x - 1]);
							}
						}
					}
					if ((y - direction) >= 0 && (y - direction) <= 8) {
						if (x < 8) {
							if (cells [y - direction, x + 1].komaState != komaState) {
								movablePoints.Add (cells [y - direction, x + 1]);
							}
						}
						if (x > 0) {
							if (cells [y - direction, x - 1].komaState != komaState) {
								movablePoints.Add (cells [y - direction, x - 1]);
							}
						}
					}
				} else {
					if ((y + direction) >= 0 && (y + direction) <= 8) {
						if (cells [y + direction, x].komaState != komaState) {
							movablePoints.Add (cells [y + direction, x]);
						}
						if (x < 8) {
							if (cells [y + direction, x + 1].komaState != komaState) {
								movablePoints.Add (cells [y + direction, x + 1]);
							}
						}
						if (x > 0) {
							if (cells [y + direction, x - 1].komaState != komaState) {
								movablePoints.Add (cells [y + direction, x - 1]);
							}
						}
					}
					if ((y - direction) >= 0 && (y - direction) <= 8) {
						if (cells [y - direction, x].komaState != komaState) {
							movablePoints.Add (cells [y - direction, x]);
						}
					}
					if (x + 1 <= 8) {
						if (cells [y, x + 1].komaState != komaState) {
							movablePoints.Add (cells [y, x + 1]);
						}
					}
					if (x - 1 >= 0) {
						if (cells [y, x - 1].komaState != komaState) {
							movablePoints.Add (cells [y, x - 1]);
						}
					}
				}
				break;
				#endregion
				#region Kin
			case Koma.Kin:
				if ((y + direction) >= 0 && (y + direction) <= 8) {
					if (cells [y + direction, x].komaState != komaState) {
						movablePoints.Add (cells [y + direction, x]);
					}
					if (x < 8) {
						if (cells [y + direction, x + 1].komaState != komaState) {
							movablePoints.Add (cells [y + direction, x + 1]);
						}
					}
					if (x > 0) {
						if (cells [y + direction, x - 1].komaState != komaState) {
							movablePoints.Add (cells [y + direction, x - 1]);
						}
					}
				}
				if ((y - direction) >= 0 && (y - direction) <= 8) {
					if (cells [y - direction, x].komaState != komaState) {
						movablePoints.Add (cells [y - direction, x]);
					}
				}
				if (x + 1 <= 8) {
					if (cells [y, x + 1].komaState != komaState) {
						movablePoints.Add (cells [y, x + 1]);
					}
				}
				if (x - 1 >= 0) {
					if (cells [y, x - 1].komaState != komaState) {
						movablePoints.Add (cells [y, x - 1]);
					}
				}
				break;
				#endregion
				#region Gyoku
			case Koma.Gyoku:
				if ((y + direction) >= 0 && (y + direction) <= 8) {
					if (cells [y + direction, x].komaState != komaState) {
						movablePoints.Add (cells [y + direction, x]);
					}
					if (x < 8) {
						if (cells [y + direction, x + 1].komaState != komaState) {
							movablePoints.Add (cells [y + direction, x + 1]);
						}
					}
					if (x > 0) {
						if (cells [y + direction, x - 1].komaState != komaState) {
							movablePoints.Add (cells [y + direction, x - 1]);
						}
					}
				}
				if ((y - direction) >= 0 && (y - direction) <= 8) {
					if (cells [y - direction, x].komaState != komaState) {
						movablePoints.Add (cells [y - direction, x]);
					}
					if (x < 8) {
						if (cells [y - direction, x + 1].komaState != komaState) {
							movablePoints.Add (cells [y - direction, x + 1]);
						}
					}
					if (x > 0) {
						if (cells [y - direction, x - 1].komaState != komaState) {
							movablePoints.Add (cells [y - direction, x - 1]);
						}
					}
				}
				if (x + 1 <= 8) {
					if (cells [y, x + 1].komaState != komaState) {
						movablePoints.Add (cells [y, x + 1]);
					}
				}
				if (x - 1 >= 0) {
					if (cells [y, x - 1].komaState != komaState) {
						movablePoints.Add (cells [y, x - 1]);
					}
				}
				break;
				#endregion
				#region Hisha
			case Koma.Hisha:
				if (!isPromoted) {
					for (int dir = 1; dir >= -1; dir -= 2) {
						for (int i = 0; y + i >= 0; i += dir) {
							if (i == 0) {
								continue;
							}
							if (i > 8) {
								i = 0;
								break;
							}

							if ((y + i) >= 0 && (y + i) <= 8) {
								if (cells [y + i, x].komaState == KomaState.None) {
									movablePoints.Add (cells [y + i, x]);
								} else if (cells [y + i, x].komaState != komaState) {
									movablePoints.Add (cells [y + i, x]);
									break;
								} else {
									i = 0;
									break;
								}
							}
						}
					}
					for (int dir = -1; dir <= 1; dir += 2) {
						for (int i = 0; x + i >= 0; i += dir) {
							if (i == 0) {
								continue;
							}
							if (i > 8) {
								i = 0;
								break;
							}

							if ((x + i) >= 0 && (x + i) <= 8) {
								if (cells [y, x + i].komaState == KomaState.None) {
									movablePoints.Add (cells [y, x + i]);
								} else if (cells [y, x + i].komaState != komaState) {
									movablePoints.Add (cells [y, x + i]);
									break;
								} else {
									i = 0;
									break;
								}
							}
						}
					}
				} else {
					for (int dir = 1; dir >= -1; dir -= 2) {
						for (int i = 0; y + i >= 0; i += dir) {
							if (i == 0) {
								continue;
							}
							if (i > 8) {
								i = 0;
								break;
							}


							if ((y + i) >= 0 && (y + i) <= 8) {
								if (cells [y + i, x].komaState == KomaState.None) {
									movablePoints.Add (cells [y + i, x]);
								} else if (cells [y + i, x].komaState != komaState) {
									movablePoints.Add (cells [y + i, x]);
									break;
								} else {
									i = 0;
									break;
								}
							}
						}
					}
					for (int dir = 1; dir >= -1; dir -= 2) {
						for (int i = 0; x + i >= 0; i += dir) {
							if (i == 0) {
								continue;
							}
							if (i > 8) {
								i = 0;
								break;
							}

							if ((x + i) >= 0 && (x + i) <= 8) {
								if (cells [y, x + i].komaState == KomaState.None) {
									movablePoints.Add (cells [y, x + i]);
								} else if (cells [y, x + i].komaState != komaState) {
									movablePoints.Add (cells [y, x + i]);
									break;
								} else {
									i = 0;
									break;
								}
							}
						}
					}
					if ((y + direction) >= 0 && (y + direction) <= 8) {
						if (x < 8) {
							if (cells [y + direction, x + 1].komaState != komaState) {
								movablePoints.Add (cells [y + direction, x + 1]);
							}
						}
						if (x > 0) {
							if (cells [y + direction, x - 1].komaState != komaState) {
								movablePoints.Add (cells [y + direction, x - 1]);
							}
						}
					}
					if ((y - direction) >= 0 && (y - direction) <= 8) {
						if (x < 8) {
							if (cells [y - direction, x + 1].komaState != komaState) {
								movablePoints.Add (cells [y - direction, x + 1]);
							}
						}
						if (x > 0) {
							if (cells [y - direction, x - 1].komaState != komaState) {
								movablePoints.Add (cells [y - direction, x - 1]);
							}
						}
					}
				}
				break;
				#endregion
				#region Kaku
			case Koma.Kaku:
				if (!isPromoted) {
					for (int dir = 1; dir >= -1; dir -= 2) {
						for (int i = 0; y + i >= 0; i += dir) {
							if (i == 0) {
								continue;
							}
							if (i > 8) {
								i = 0;
								break;
							}


							if ((y + i) >= 0 && (y + i) <= 8) {
								if ((x + i) >= 0 && (x + i) <= 8) {
									if (cells [y + i, x + i].komaState == KomaState.None) {
										movablePoints.Add (cells [y + i, x + i]);
									} else if (cells [y + i, x + i].komaState != komaState) {
										movablePoints.Add (cells [y + i, x + i]);
										break;
									} else {
										i = 0;
										break;
									}
								}
							}
						}
					}
					for (int dir = -1; dir <= 1; dir += 2) {
						for (int i = 0; x + i >= 0; i += dir) {
							if (i == 0) {
								continue;
							}
							if (i > 8) {
								i = 0;
								break;
							}

							if ((y - i) >= 0 && (y - i) <= 8) {
								if ((x + i) >= 0 && (x + i) <= 8) {
									if (cells [y - i, x + i].komaState == KomaState.None) {
										movablePoints.Add (cells [y - i, x + i]);
									} else if (cells [y - i, x + i].komaState != komaState) {
										movablePoints.Add (cells [y - i, x + i]);
										break;
									} else {
										i = 0;
										break;
									}
								}
							}
						}
					}
				} else {
					for (int dir = 1; dir >= -1; dir -= 2) {
						for (int i = 0; y + i >= 0; i += dir) {
							if (i == 0) {
								continue;
							}
							if (i > 8) {
								i = 0;
								break;
							}

							if ((y + i) >= 0 && (y + i) <= 8) {
								if ((x + i) >= 0 && (x + i) <= 8) {
									if (cells [y + i, x + i].komaState == KomaState.None) {
										movablePoints.Add (cells [y + i, x + i]);
									} else if (cells [y + i, x + i].komaState != komaState) {
										movablePoints.Add (cells [y + i, x + i]);
										break;
									} else {
										i = 0;
										break;
									}
								}
							}
						}
					}
					for (int dir = 1; dir >= -1; dir -= 2) {
						for (int i = 0; x + i >= 0; i += dir) {
							if (i == 0) {
								continue;
							}
							if (i > 8) {
								i = 0;
								break;
							}

							if ((y - i) >= 0 && (y - i) <= 8) {
								if ((x + i) >= 0 && (x + i) <= 8) {
									if (cells [y - i, x + i].komaState == KomaState.None) {
										movablePoints.Add (cells [y - i, x + i]);
									} else if (cells [y - i, x + i].komaState != komaState) {
										movablePoints.Add (cells [y - i, x + i]);
										break;
									} else {
										i = 0;
										break;
									}
								}
							}
						}
					}
					if ((y + direction) >= 0 && (y + direction) <= 8) {
						if (cells [y + direction, x].komaState != komaState) {
							movablePoints.Add (cells [y + direction, x]);
						}
					}
					if ((y - direction) >= 0 && (y - direction) <= 8) {
						if (cells [y - direction, x].komaState != komaState) {
							movablePoints.Add (cells [y - direction, x]);
						}
					}
					if (x + 1 <= 8) {
						if (cells [y, x + 1].komaState != komaState) {
							movablePoints.Add (cells [y, x + 1]);
						}
					}
					if (x - 1 >= 0) {
						if (cells [y, x - 1].komaState != komaState) {
							movablePoints.Add (cells [y, x - 1]);
						}
					}
				}
				break;
				#endregion
			}
		} else {
			for (int j = 0; j < 9; j++) {
				for (int i = 0; i < 9; i++) {
					if (j == 7) {
						if (koma == Koma.Kei) {
							return movablePoints;
						}
					}
					if (j == 8) {
						if (koma == Koma.Hu || koma == Koma.Kyou) {
							return movablePoints;
						}
					}
					if (cells [frontLine + direction * j, i].komaState == KomaState.None) {
						movablePoints.Add (cells [frontLine + direction * j, i]);
					}
				}
			}
		}

        return movablePoints;
    }
    #endregion
}
