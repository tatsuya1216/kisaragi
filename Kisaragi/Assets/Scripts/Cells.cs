﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cells : MonoBehaviour {

    public Cell[,] cells;						// 盤上のセル
	public Cell[,] cellsOnTable;				// 持ち駒のセル cellsOnTable[0,]:先手の持ち駒
    public Vector3[,] cellPositionsOnBoard;     // cell配列とボード上の位置
	public Vector3[,] cellPositionsOnTable;		// cell配列と持ち駒台上の位置
    public int colNum, rowNum;      //colNum:列 rowNum:行
    private int yZero = 300;        //1,1のマスのY座標
    private int xZero = 300;        //1,1のマスのX座標
	private Vector3 startPosOnBlackTable = new Vector3(400, -150);		// 先手持ち駒台の左上の座標
	private Vector3 startPosOnWhiteTable = new Vector3(-400, 150);		// 後手		  ``
	public int tableCol = 5;										// 持ち駒台の一列中の駒数
	public int tableRow = 5;										// 持ち駒台の一行中の駒数

    public void Init()
    {
        colNum = 9;
        rowNum = 9;
        cells = new Cell[colNum, rowNum];
        cellPositionsOnBoard = new Vector3[colNum, rowNum];
        for (int y = 0; y < colNum; y++)
        {
            for (int x = 0; x < rowNum; x++)
            {
                cellPositionsOnBoard[y, x] = new Vector3(xZero - (75 * x), yZero - (75* y), 0);
            }
        }

		// 持ち駒台
		cellsOnTable = new Cell[2,tableCol*tableRow];
		cellPositionsOnTable = new Vector3[2, tableCol * tableRow];
		int count = 0;
		for (int y = 0; y < tableCol; y++) {
			for (int x = 0; x < tableRow; x++) {
				cellPositionsOnTable [0, count] = startPosOnBlackTable + new Vector3 (50 * x, -50 * y, 0);
				count++;
			}
		}

		count = 0;
		for (int y = 0; y < tableCol; y++) {
			for (int x = 0; x < tableRow; x++) {
				cellPositionsOnTable [1, count] = startPosOnWhiteTable + new Vector3 (-50 * x, 50 * y, 0);
				count++;
			}
		}
    } 

    public void StandardInitialize()    //平手
    {
        int center = colNum / 2;
        Cell.KomaState komaState;

        for (int n = -1; n < 2; n += 2)
        {
            komaState = (n == 1) ? Cell.KomaState.Black : Cell.KomaState.White;

            cells[center + (center - 0) * n, 0].SetCellInitialize(Cell.Koma.Kyou, komaState, false, center + (center - 0) * n, 0);
            cells[center + (center - 0) * n, 1].SetCellInitialize(Cell.Koma.Kei, komaState, false, center + (center - 0) * n, 1);
            cells[center + (center - 0) * n, 2].SetCellInitialize(Cell.Koma.Gin, komaState, false, center + (center - 0) * n, 2);
            cells[center + (center - 0) * n, 3].SetCellInitialize(Cell.Koma.Kin, komaState, false, center + (center - 0) * n, 3);
            cells[center + (center - 0) * n, 4].SetCellInitialize(Cell.Koma.Gyoku, komaState, false, center + (center - 0) * n, 4);
            cells[center + (center - 0) * n, 5].SetCellInitialize(Cell.Koma.Kin, komaState, false, center + (center - 0) * n, 5);
            cells[center + (center - 0) * n, 6].SetCellInitialize(Cell.Koma.Gin, komaState, false, center + (center - 0) * n, 6);
            cells[center + (center - 0) * n, 7].SetCellInitialize(Cell.Koma.Kei, komaState, false, center + (center - 0) * n, 7);
            cells[center + (center - 0) * n, 8].SetCellInitialize(Cell.Koma.Kyou, komaState, false, center + (center - 0) * n, 8);
			cells[center + (center - 1) * n, center - (center - 1) * n].SetCellInitialize(Cell.Koma.Hisha, komaState, false, center + (center - 1) * n, center - (center - 1) * n);
			cells[center + (center - 1) * n, center + (center - 1) * n].SetCellInitialize(Cell.Koma.Kaku, komaState, false, center + (center - 1) * n, center + (center - 1) * n);
            for (int i = 0; i < rowNum; i++)
            {
                cells[center + (center - 2) * n, i].SetCellInitialize(Cell.Koma.Hu, komaState, false, center + (center - 2) * n, i);
            }
        }
    }




}
