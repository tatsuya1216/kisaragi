﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Board : MonoBehaviour {

    [SerializeField]
	private GameObject board;
	[SerializeField]
	private GameObject komaParent = null;
	[SerializeField]
	private GameObject cellParent = null;
    [SerializeField]
	private GameObject tapHghlightParent = null;
    [SerializeField]
	private GameObject tapHighlight = null;
	[SerializeField]
	private GameObject tappedKoma = null;
	[SerializeField]
	private GameObject blackLayer = null;
	[SerializeField]
	private GameObject promoteDialogParent = null;
    [SerializeField]
    private GameObject Hu = null;
    [SerializeField]
	private GameObject Kyou = null;
    [SerializeField]
	private GameObject Kei = null;
    [SerializeField]
	private GameObject Gin = null;
    [SerializeField]
	private GameObject Kin = null;
    [SerializeField]
	private GameObject Gyoku = null;
    [SerializeField]
	private GameObject Hisha = null;
    [SerializeField]
	private GameObject Kaku = null;

    public GameObject cell;
    public GameObject[,] komasOnBoard; //cells[y,x] 9x9 (0~8)
	public GameObject[,] komasOnTable; //持ち駒komas[0,]:先手の持ち駒 komas[1,]:後手の持ち駒

	private Vector3 tappedPosition;
    private Cell tappedCell;
    private List<Cell> movablePoints = new List<Cell>();

	public bool promote = false;
	public bool promote_cancel = false;

	#region Init
    public void Init ()
    {

        int colNum = BoardManager.Instance.cells.colNum;
        int rowNum = BoardManager.Instance.cells.rowNum;
        for (int y = 0; y < colNum; y++)
        {
            for (int x = 0; x < rowNum; x++)
            {
                GameObject currentObject;
                Cell currentCell;

                currentObject = (GameObject)Instantiate(cell);
                currentObject.GetComponent<Input>().SetCordinate(y, x);
                currentCell = currentObject.GetComponent<Cell>();
                currentCell.Init();

				currentObject.transform.SetParent(cellParent.transform);
                currentObject.transform.localScale = new Vector3(1, 1, 1);
                currentObject.transform.localPosition = BoardManager.Instance.cells.cellPositionsOnBoard[y, x];

                BoardManager.Instance.cells.cells[y, x] = currentCell;
                BoardManager.Instance.cells.cells[y, x].SetCellInitialize(Cell.Koma.None, Cell.KomaState.None, false, y, x);
            }
        }

		int tableCol = BoardManager.Instance.cells.tableCol;
		int tableRow = BoardManager.Instance.cells.tableRow;
		for (int j = 0; j < 2; j++)
		{
			for (int i = 0; i < tableCol*tableRow; i++)
			{
				GameObject currentObject;
				Cell currentCell;

				currentObject = (GameObject)Instantiate(cell);
				currentObject.GetComponent<Input>().SetCordinate(j, i);
				currentCell = currentObject.GetComponent<Cell>();
				currentCell.Init();

				currentObject.transform.SetParent(cellParent.transform);
				currentObject.transform.localScale = new Vector3(1, 1, 1);
				currentObject.transform.localPosition = BoardManager.Instance.cells.cellPositionsOnTable[j, i];

				BoardManager.Instance.cells.cellsOnTable[j, i] = currentCell;
				BoardManager.Instance.cells.cellsOnTable[j, i].SetCellInitialize(Cell.Koma.None, Cell.KomaState.None, false, j, i, Cell.KomaPlace.Table);
			}
		}

		komasOnTable = new GameObject[2, 40];
    }
	#endregion

	#region コマの整列
    public void SetInitialKoma()
    {
        int colNum = BoardManager.Instance.cells.colNum;
        int rowNum = BoardManager.Instance.cells.rowNum;
        Cell[,] cells = BoardManager.Instance.cells.cells;
        komasOnBoard = new GameObject[colNum, rowNum];
        for (int y = 0; y < colNum; y++)
        {
            for (int x = 0; x < rowNum; x++)
            {
                Cell.Koma koma = cells[y, x].koma;
                switch(koma)
                {
                    case Cell.Koma.Hu:
                        komasOnBoard[y, x] = (GameObject)Instantiate(Hu);
                        break;
                    case Cell.Koma.Kyou:
                        komasOnBoard[y, x] = (GameObject)Instantiate(Kyou);
                        break;
                    case Cell.Koma.Kei:
                        komasOnBoard[y, x] = (GameObject)Instantiate(Kei);
                        break;
                    case Cell.Koma.Gin:
                        komasOnBoard[y, x] = (GameObject)Instantiate(Gin);
                        break;
                    case Cell.Koma.Kin:
                        komasOnBoard[y, x] = (GameObject)Instantiate(Kin);
                        break;
                    case Cell.Koma.Gyoku:
                        komasOnBoard[y, x] = (GameObject)Instantiate(Gyoku);
                        break;
                    case Cell.Koma.Hisha:
                        komasOnBoard[y, x] = (GameObject)Instantiate(Hisha);
                        break;
                    case Cell.Koma.Kaku:
                        komasOnBoard[y, x] = (GameObject)Instantiate(Kaku);
                        break;
                    default:
                        continue;
                }

				komasOnBoard[y, x].transform.SetParent(komaParent.transform);
                komasOnBoard[y, x].transform.localScale = Vector3.one;
                komasOnBoard[y, x].transform.localPosition = BoardManager.Instance.cells.cellPositionsOnBoard[y, x];

                if (cells[y,x].komaState == Cell.KomaState.White)
                {
                    komasOnBoard[y, x].transform.localRotation = Quaternion.Euler(0, 0, 180);
                }
            }
        }
    }
	#endregion

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	#region コマを選択
	public void TapHighlight (int y, int x, Cell cell)
    {

		if (cell == null) {
			//コマを選択中になんでもないところをタップして選択を解除
			foreach (Transform child in tapHghlightParent.transform) {
				Destroy(child.gameObject);
			}
			movablePoints.Clear ();
			//黒レイヤーの消去
			blackLayer.SetActive(false);

			if (tappedKoma.transform.childCount > 0) {
				tappedKoma.transform.GetChild (0).transform.SetParent (komaParent.transform);
			}

			return;
		}

		Vector3 position;
		if (cell.komaPlace == Cell.KomaPlace.Board) {
			position = BoardManager.Instance.cells.cellPositionsOnBoard [y, x];
		} else {
			position = BoardManager.Instance.cells.cellPositionsOnTable [y, x];
		}
			
		//選択したマスに駒がない場合
        if (cell.koma == Cell.Koma.None)
        {
			if (tapHghlightParent.transform.childCount != 0) {
				//コマがないマスへの着手
				if (movablePoints.Contains (cell)) {
					//Debug.Log("Move!!!");
					BoardManager.Instance.Move (cell, tappedCell);
				}

				//コマを選択中になんでもないところをタップして選択を解除
				foreach (Transform child in tapHghlightParent.transform) {
					Destroy(child.gameObject);
				}
				movablePoints.Clear ();
				//黒レイヤーの消去
				blackLayer.SetActive(false);

				if (tappedKoma.transform.childCount > 0) {
					tappedKoma.transform.GetChild (0).transform.SetParent (komaParent.transform);
				}
			}
        }
		//選択したマスに駒がある場合
        else
        {
			if (tapHghlightParent.transform.childCount != 0)
			{
				foreach (Transform child in tapHghlightParent.transform) {
					Destroy(child.gameObject);
				}
				//黒レイヤーの消去
				blackLayer.SetActive(false);

				if (tappedKoma.transform.childCount > 0) {
					tappedKoma.transform.GetChild (0).transform.SetParent (komaParent.transform);
				}
				if (cell.komaState != tappedCell.komaState) {
					//着手 + 相手のコマをとる
					if (movablePoints.Contains(cell))
					{
						BoardManager.Instance.Move(cell, tappedCell);
						movablePoints.Clear();
						return;
					}
				}
                movablePoints.Clear();
				if (tappedPosition == position) {
					return;
				} else {
					tappedPosition = position;
				}
            }
				
			//動かしたいコマの選択
			if ((cell.komaState == Cell.KomaState.Black && GameManager.Instance.turn == GameManager.Turn.Black) || (cell.komaState == Cell.KomaState.White && GameManager.Instance.turn == GameManager.Turn.White)) {
				movablePoints = cell.MovablePoints (y, x, cell);

				//tapHighlightの生成
/*				tap = (GameObject)Instantiate (tapHighlight);
				tap.transform.SetParent (tapHghlightParent.transform);
				tap.transform.localScale = new Vector3 (1, 1, 1);
				tap.transform.localPosition = position;
*/				
				tappedCell = cell;

				foreach (Cell movableCell in movablePoints) {
					GameObject tapH = (GameObject)Instantiate (tapHighlight);
					tapH.transform.SetParent (tapHghlightParent.transform);
					tapH.transform.localScale = new Vector3 (1, 1, 1);
					tapH.transform.localPosition = BoardManager.Instance.cells.cellPositionsOnBoard [movableCell.yIndex, movableCell.xIndex];
				}

				if (!blackLayer.activeSelf) {
					//黒レイヤーの生成
					blackLayer.SetActive (true);
				}

				if (cell.komaPlace == Cell.KomaPlace.Board) {
					komasOnBoard [y, x].transform.SetParent (tappedKoma.transform);
				} else {
					komasOnTable [y, x].transform.SetParent (tappedKoma.transform);
				}

			} else {
				return;
			}
        }
    }
	#endregion

	#region 着手
	// BoardManagerのなかで呼ばれる。こっちは駒の移動のみ。
	public IEnumerator Move(Cell targetCell, Cell tappedCell)	
    {
        int nowY = tappedCell.yIndex;
        int nowX = tappedCell.xIndex;
        int nextY = targetCell.yIndex;
        int nextX = targetCell.xIndex;
		int r = 0;
		int blackOrWhite = tappedCell.komaState == Cell.KomaState.Black ? 0 : 1;

		if (tappedCell.komaPlace == Cell.KomaPlace.Board) {

			if (!tappedCell.isPromoted && BoardManager.Instance.cells.cells [nextY, nextX].CheckPromoteZone ()) {
				promoteDialogParent.SetActive (true);
				yield return new WaitWhile ( () => (!promote && !promote_cancel));
				if (promote) {
					BoardManager.Instance.Promote (BoardManager.Instance.cells.cells [nowY, nowX], true);
				} else if (promote_cancel) {
					//何もしない
				}
				promote = promote_cancel = false;
			}

			if (targetCell.komaState != tappedCell.komaState && targetCell.komaState != Cell.KomaState.None) {
				while (true) {
					r = Random.Range (0, 24);
					if (komasOnTable [blackOrWhite, r] == null) {
						komasOnTable [blackOrWhite, r] = komasOnBoard [nextY, nextX];
						komasOnBoard [nextY, nextX].transform.localPosition = BoardManager.Instance.cells.cellPositionsOnTable [blackOrWhite, r];
						komasOnBoard [nextY, nextX].transform.localRotation = Quaternion.Euler (0, 0, 180 * blackOrWhite);
						BoardManager.Instance.cells.cellsOnTable [blackOrWhite, r].SetCell (targetCell.koma, tappedCell.komaState, false, Cell.KomaPlace.Table);		// 駒大に行くときは成りを解除
						BoardManager.Instance.Promote (BoardManager.Instance.cells.cellsOnTable [blackOrWhite, r], false);
						break;
					}
				}
			}

			komasOnBoard [nextY, nextX] = komasOnBoard [nowY, nowX];
			komasOnBoard [nowY, nowX] = null;
			komasOnBoard [nextY, nextX].transform.localPosition = BoardManager.Instance.cells.cellPositionsOnBoard [nextY, nextX];

			BoardManager.Instance.cells.cells [nextY, nextX].SetCell (tappedCell.koma, tappedCell.komaState, tappedCell.isPromoted);
			BoardManager.Instance.cells.cells [nowY, nowX].SetCell (Cell.Koma.None, Cell.KomaState.None, false);
		} else {
			komasOnBoard [nextY, nextX] = komasOnTable [nowY, nowX];
			komasOnTable [nowY, nowX] = null;
			komasOnBoard [nextY, nextX].transform.localPosition = BoardManager.Instance.cells.cellPositionsOnBoard [nextY, nextX];

			BoardManager.Instance.cells.cells [nextY, nextX].SetCell (tappedCell.koma, tappedCell.komaState, tappedCell.isPromoted, Cell.KomaPlace.Board);
			BoardManager.Instance.cells.cellsOnTable [nowY, nowX].SetCell (Cell.Koma.None, Cell.KomaState.None, false);
		}
    }
	#endregion

}
