﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KomaInfo : MonoBehaviour {

	[SerializeField]GameObject _child = null;	//成じゃないほう
	[SerializeField]GameObject n_child = null;	//成のほう

	public void Init () {
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	// ただひっくり返す
	#region Promote
	public bool Promote(bool isPromote) {
		if (n_child == null) {
			if (isPromote) {
				return false;
			} else {
				_child.SetActive (!isPromote);
				return true;
			}
		} else {
			_child.SetActive (!isPromote);
			n_child.SetActive (isPromote);
			return true;
		}
	}
	#endregion
}
