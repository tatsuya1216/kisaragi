﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardManager : SingletonMonoBehaviour<BoardManager> {
    
    public Cells cells;
    public Board board;

    protected override void Init()
    {
        cells = this.GetComponent<Cells>();

        cells.Init();
        board.Init();
        cells.StandardInitialize();
        board.SetInitialKoma();

    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

    }

    public void Move(Cell targetCell, Cell tappedCell)
    {
        int nowY = tappedCell.yIndex;
        int nowX = tappedCell.xIndex;
        int nextY = targetCell.yIndex;
        int nextX = targetCell.xIndex;

		StartCoroutine(board.Move(targetCell, tappedCell));

		GameManager.Instance.ChangeTurn (GameManager.Instance.turn);

//		cells.cells[nextY, nextX].SetCell(tappedCell.koma, tappedCell.komaState, tappedCell.isPromoted);
//        cells.cells[nowY, nowX].SetCell(Cell.Koma.None, Cell.KomaState.None, false);
//
//		if (cells.cells [nextY, nextX].CheckPromoteZone ()) {
//			Promote (cells.cells [nextY, nextX], true);
//		}
    }

	public void Promote(Cell cell, bool isPromoted) {
		int y = cell.yIndex;
		int x = cell.xIndex;
		KomaInfo ki;
		if (cell.komaPlace == Cell.KomaPlace.Board) {
			ki = board.komasOnBoard [y, x].gameObject.GetComponent<KomaInfo> ();
		} else {
			ki = board.komasOnTable [y, x].gameObject.GetComponent<KomaInfo> ();
		}

		bool promoteSuccessed = ki.Promote (isPromoted);
		cell.Promote (isPromoted && promoteSuccessed);
	}

	public void TapHighlight (int y, int x, Cell cell)
    {
		board.TapHighlight(y, x, cell);
    }
}
