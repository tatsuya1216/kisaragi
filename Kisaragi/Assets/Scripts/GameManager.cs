﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : SingletonMonoBehaviour<GameManager> {

	public enum Turn
	{
		Black,	//先手
		White	//後手
	}

	public enum Teai
	{
		
	}

    public Cell cell;
	public Turn turn = Turn.Black;

    protected override void Init()
    {
        cell = this.GetComponent<Cell>();
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void SetGameInfo (Turn firstTurn) 
	{
		this.turn = firstTurn;
	}

	public Turn ChangeTurn (Turn currentTurn) 
	{
		switch (currentTurn) 
		{
		case Turn.Black:
			this.turn = Turn.White;
			break;
		case Turn.White:
			this.turn = Turn.Black;
			break;
		default:
			break;
		}

		return this.turn;
	}
}
